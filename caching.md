# Caching

Caching is a process that stores copies of data or files in a temporary storage location—or cache—so they can be accessed faster. It temporarily saves data for applications, servers, and web browsers, which ensures users need not download information every time they access a website or application.

## Why do we need caching

### _Effective caching aids both content consumers and content providers. Some of the benefits that caching brings to content delivery are:_

- **Decreased network costs:** When the content is cached closer to the consumer, requests will not cause much additional network activity beyond the cache.

- **Improved responsiveness:** Caching enables content to be retrieved faster because an entire network round trip is not necessary.

- **Increased performance on the same hardware:** For the server where the content originated, more performance can be squeezed from the same hardware by allowing aggressive caching.

- **Availability of content during network interruptions:** With certain policies, caching can be used to serve content to end users even when it may be unavailable for short periods of time from the origin servers.

## Types of Caching

1. **Database Caching:** A database cache supplements your primary database by removing unnecessary pressure on it, typically in the form of frequently accessed read data,also lower the data retrival latency and improves the overall performance of your applications.

2. **Content Delivery Network(CDN) Caching:** The CDN utilizes the nearest edge location to the customer or originating request location in order to reduce the load on an application origin and improve the experience of the requestor by delivering a local copy of the content from a nearby cache edge.

3. **Web Caching:** Web caching is performed by retaining HTTP responses and web resources in the cache for the purpose of fulfilling future requests from cache rather than from the origin servers.

4. **General Caching:** General caching uses in-memory key-value store which is faster than accessing data from disk or SSD, so leveraging data in cache has a lot of advantages.

5. **Session Management:** Session management includes storing sessions locally to the node responding to the HTTP request or designating a layer in an architecture which can store those sessions in a scalable and robust manner.

## Different Caching strategies

### _A caching strategy is to determine the relationship between data source and your caching system, and how your data can be accessed_

- **Read Through / Lazy Loading:** Load data into the cache only when necessary. If application needs data, search in the cache first. If data is present, return the data, otherwise, retrieve the data from data source, put it into the cache & then return.

- **Write Through:** Similar to read through, the cache sits in between. Every writes from the application must go through the cache to the database. So both of these operations should occur in a single transaction otherwise data staleness will be there.

- **Write Behind:** The application writes data directly to the caching system. Then after a certain configured interval, the written data is asynchronously synced to the underlying data source.

- **Write Around:** The application writes directly to the database. Only data that is read goes to the cache.

## Summary

Caching is a reliable and low-hassle way to improve your pages' load speed and thus your users' experience. It is powerful enough to allow sophisticated nuances for specific content types, but flexible enough to allow easy updates when your site's content changes.

### _References:_

[1]. <https://aws.amazon.com/caching/>

[2]. <https://ibit.ly/hTCC>

[3]. <https://ibit.ly/aPI7>
